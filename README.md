# mydummyapp

## Synopsis
This repository holds two Maven projects to demonstrate the MVC, IoC/DI and other persistence-related patterns.

## Directory structure

* mydummyapp-hibernate: it uses Spring Boot with Spring Web, Thymeleaf and Hibernate.
* mydummyapp-springdata: it uses Spring Boot with Spring Web, Thymeleaf and Spring Data.

Read the README.md into each directory to get more information about how to run these projects and to access other useful links.

## License
This project is licensed under the AGPLv3. See the [LICENSE](LICENSE) file for details.
