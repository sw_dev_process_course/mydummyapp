package it.unimib.disco.lta.mydummyapphibernate.repository;

import it.unimib.disco.lta.mydummyapphibernate.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public Optional<User> findById(Long id) {
    User user = entityManager.find(User.class, id);
    return Optional.ofNullable(user);
  }

  @Override
  public Iterable<User> findAll() {
    List<User> users = entityManager.createQuery("FROM User", User.class).getResultList();
    return users;
  }

}
