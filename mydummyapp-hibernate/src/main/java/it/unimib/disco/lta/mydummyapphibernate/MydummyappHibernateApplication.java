package it.unimib.disco.lta.mydummyapphibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MydummyappHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(MydummyappHibernateApplication.class, args);
	}

}
