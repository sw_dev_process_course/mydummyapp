package it.unimib.disco.lta.mydummyapphibernate.repository;

import it.unimib.disco.lta.mydummyapphibernate.model.User;

public interface UserRepository extends Repository<User, Long>{
}
