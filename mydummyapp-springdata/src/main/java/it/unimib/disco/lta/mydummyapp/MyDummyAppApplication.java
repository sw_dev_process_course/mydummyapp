package it.unimib.disco.lta.mydummyapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyDummyAppApplication {
 
  public static void main(String[] args) {
    SpringApplication.run(MyDummyAppApplication.class, args);
  }

}
