# mydummyapp-springdata

## How to execute
### Package the app
```bash
./mvnw clean package spring-boot:repackage
```
### Run the app
```bash
java -Djava.security.egd=file:/dev/./urandom -jar target/mydummyapp-springdata.jar
```
The app will be available at [http://localhost:8080/users](http://localhost:8080/users).

### Execute with Docker
You can also execute the app within a Docker container.

First, you need to package the app with the command above.
Then, you can run the build process:

```bash
docker build -t mydummyapp-springdata:0.1.0-SNAPSHOT .
```

Finally, you can run a container:

```bash
docker run --rm -p 8080:8080 mydummyapp-springdata:0.1.0-SNAPSHOT
```
The app will be available at [http://localhost:8080/users](http://localhost:8080/users).

### Reference Documentation
For further reference, please consider the following sections:
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-spring-mvc-template-engines)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
